Rap.define('/global.js', ["/page_info.js","/ly/init.js"], function (page_info) {



    Ly.config({
        uploadUrl: "/home/upload",//图片直传地址
        uploadDataUrl: "",//
        pageInfo:page_info,
        simditor: {
            upload: {url:'/home/upload',dataUrl: "", fileKey: 'file'},
            toolbar: ['title', 'bold', 'italic', 'underline', 'strikethrough', 'fontScale', 'color', 'ol', 'ul', 'blockquote', 'code', 'table', 'link', 'image', 'hr', 'indent', 'outdent', 'alignment'],
            toolbarFloatOffset:"10px"
        }
    });

    Rap.ready(function () {
        return  axios.get("/portal/menu.json").then(function (rs) {
            return  rs.data;
        });
    }).then(function (menusData){
        //权限这里是本地测试,正式环境请使用服务端返回数据
        var role={
            code:'super',
            resources:[],
        };
        var test_role=localStorage.getItem('ly-test-role')
        if(test_role){
            try {
                role = JSON.parse(test_role);
            }catch (e){
            }
            if(!role.resources)role.resources=[];
            role.resources.push('util');
            role.resources.push('staff.role');
        }
        Ly.role.config({
            role: role.code,//可以换成其他角色查看权限
            menus:menusData,
            resources:role.resources
        });
        Ly.app({
            data: function () {
                var me=this;
                return {
                    menusData:menusData,
                    configSearchData:[{"link":"/form/basic_form?","name":"综合实例","labels":["商品名称","长文本说明","字数限制","前缀","后缀","单选","多选","前缀选择","radio","radio按钮","radio框","checkbox","checkbox框","数字选择","时间","时间范围","颜色选择器","进度条","进度环"]}],
                    globalSearch: function (text) {
                        return [{
                            title: '功能点',
                            content: new Promise(function (resolve, reject){
                                var data = [];
                                if(!text)return [];
                                _.each(me.menusData, function (cate) {
                                    _.each(cate.menus, function (menu) {
                                        _.each(menu.children, function (m) {
                                            if ((m.name && m.name.indexOf(text) > -1) ||
                                                (m.title && m.title.indexOf(text) > -1) ||
                                                (m.keywords && m.keywords.indexOf(text) > -1)
                                            ) {
                                                data.push({
                                                    name: m.name,
                                                    link: m.link
                                                })
                                            }
                                        });
                                    });
                                });
                                resolve(data);
                            })
                        },{
                            title:'配置项',
                            content:new Promise(function (resolve, reject){
                                   var data=[];
                                   if(!text)return [];
                                    _.each(me.configSearchData, function (item) {
                                        if(item.name.indexOf(text)>-1){
                                            data.push({name:item.name,link:item.link})
                                        }
                                        _.each(item.labels, function (label) {
                                            if(label.indexOf(text)>-1) {
                                                data.push({
                                                    name: label,
                                                    link: item.link + "&form_label_search=" + label
                                                })
                                            }
                                        });
                                    });
                                    resolve(data)
                                })
                        }, {
                            title: '文档',
                            content: axios.get('/home/docToc')
                                .then(function (rs) {
                                        var menu1 = rs.data;
                                        var data = [];
                                        _.each(menu1, function (m) {
                                            if (m.title && m.title.indexOf(text) > -1) {
                                                data.push({
                                                    name: m.title,
                                                    link: "https://www.yuque.com/yuque/ng1qth/" + m.url
                                                })
                                            }
                                        });
                                        return data;
                                    }
                                )
                        }];
                    }
                }
            },
            methods: {
                toMenuCategory:function (category){
                    Ly.role.toMenuCategory(category);
                }
            },
            created: function () {

            }
        });
    });
});


