Rap.define('/ly/utils/filters.js',[],function () {

    Vue.filter('trim', function (value) {
        if(!value)return "";
        return value.trim();
    });
    /**
     * 分转化为元
     */
    Vue.filter('price', function (value) {
        return '¥' + (value / 100).toFixed(2);
    });

    /**
     * 性别 1 男 2 女
     */
    Vue.filter('sex', function (value) {
        var sexes = ["未知", "男", "女"];
        return sexes[value];
    });

    Vue.filter('split', function (value, type) {
        if (!type) {
            type = '，';
        }
        if (!value) {
            value = [];
        }
        return value.join(type);
    });

    /**
     * 如果空 显示-
     */
    Vue.filter('blank', function (value) {
        if(!value||value==''){
            return '-'
        }
        return value;
    });


    Vue.filter('date', function (value) {
        if (!value)return '';
        if (typeof(value) == 'number') {
            if (value < 2000000000) {
                return dayjs(value * 1000).format('YYYY-MM-DD')
            } else {
                return dayjs(value).format('YYYY-MM-DD')
            }
        } else {
            return dayjs(value).format('YYYY-MM-DD')
        }
    });
    Vue.filter('datetime', function (value) {
        if (typeof(value) == 'number') {
            if (value < 2000000000) {
                return dayjs(value * 1000).format('YYYY-MM-DD HH:mm')
            } else {
                return dayjs(value).format('YYYY-MM-DD HH:mm')
            }
        } else {
            return dayjs(value).format('YYYY-MM-DD HH:mm')
        }
    });

    /**
     * 转年龄
     */
    Vue.filter('age', function (value) {
        if (!value)return '';
        var month = dayjs().diff(dayjs(value), 'month');
        var year = Math.floor(month / 12);
        month = month % 12;
        var str = '';
        if (year === 0 && month === 0) {
            return dayjs().diff(dayjs(value), 'week') + '周';
        }
        if (year > 0) {
            str += year + '岁';
        }
        if (month === 0) {
            return str;
        }
        if (month < 10) {
            month = '0' + month;
        }
        return str + ' ' + month + '个月';
    });


    /**
     * 转最近时间
     */
    Vue.filter('near_time', function (value) {
        var day_time = '';
        if (typeof(value) == 'number') {
            if (value < 2000000000) {
                day_time = dayjs(value * 1000).format('YYYY-MM-DD HH:mm')
            } else {
                day_time = dayjs(value).format('YYYY-MM-DD HH:mm')
            }
        } else {
            day_time = dayjs(value).format('YYYY-MM-DD HH:mm')
        }

        var second = new Date().getTime() / 1000 - value;
        if (second < 0) {
            return day_time;
        }
        if (second < 60) {
            return second + "秒";
        }
        var m = parseInt(second / 60);
        if (m < 60) {
            return m + "分钟";
        }
        if (m < 60 * 24) {
            var h = parseInt(second / 60 / 60);
            return h + "小时前";
        }
        return day_time;
    });
    /**
     * 星期
      */
    Vue.filter('week', function (value) {
        if (value) {
            var arr = ['周日', '周一', '周二', '周三', '周四', '周五', '周六'];
            value = arr[value];
        }
        return value;
    });
});