Rap.define('/ly/utils/guide.js', [], function () {
    var _guides = [];
    var _is_show=false;
    var _option=null;
    var _modelV=null;
    var _nextClickModel=null;
    return {
        show: function (option) {
            _option=option;
            var can_finish = option.can_finish;
            var callback = option.callback;
            if (option.guides) {
                for (var i=0;i < option.guides.length;i++) {
                    var info = option.guides[i];
                    var el=info.el;
                    if(!el){
                        el = document.querySelectorAll([info.selector])[0];
                    }
                    if(!el||getStyle(el,'display')==='none'){
                        continue;
                    }
                    el.setAttribute('guide-content', info.content);
                    if(info.placement){
                        el.setAttribute('guide-placement',info.placement);
                    }
                    if(info.placement){
                        el.setAttribute('guide-placement',info.placement);
                    }
                    if(info.group){
                        el.setAttribute('guide-group',info.group);
                    }
                    if(info.title){
                        el.setAttribute('guide-title',info.title);
                    }
                    if(info.level){
                        el.setAttribute('guide-level',info.level);
                    }
                }
            }

            var selector='[guide-content]';
            if(option.group){
                selector+='[guide-group='+option.group+']';
            }else{
                selector+=':not([guide-group])';
            }
            var guides = document.querySelectorAll([selector]);
            _guides=[];
            var guides_map={};
            var level_def=10000;
            for(let i=0;i<guides.length;i++){
                let guide_el= guides[i];
                let level=guide_el.getAttribute('guide-level');
                if(!level){
                    level=level_def;
                    level_def++;
                }
                guides_map[level]=guide_el;
            }

            var key_map=Object.keys(guides_map).sort();
            for(let i=0;i<key_map.length;i++){
                _guides.push(guides_map[key_map[i]]);
            }
            if (_is_show) {
                return;
            }
            _is_show = true;
            var me = this;
            var step = -1;
            var div = document.createElement('div');
            div.innerHTML = '<div style="box-shadow:  #0000007a 0 0 0 0,  #0000007a 0 0 0 5000px;position: absolute;z-index: 1000"></div>';
            var child = div.children[0]
            child.remove();
            _modelV = child;
            document.body.append(child);
            div.innerHTML = '<div style="position: fixed;left:0;right:0;bottom:0;top:0;z-index: 1001"></div>';
            _nextClickModel = div.children[0]
            _nextClickModel.remove();
            document.body.append(_nextClickModel);
            div.innerHTML = '<div class="ly-guide-popover el-popover el-popper el-popover--plain" tabindex="0" style="position: absolute;top: 0;left: 0;transform-origin: center top;z-index: 1003;max-width: 460px;" x-placement="right"><div class="el-popover__title">标题</div><span class="popover__content">哈哈哈</span> <div style="text-align: right;margin-bottom: -10px;margin-top: 10px;font-size: 12px;"><span class="ly-guide-close" style="margin-right: 10px;cursor: pointer;color: #182026">关闭</span><span class="ly-guide-before" style="margin-right: 10px;cursor: pointer;color: var(--primary)">上一条</span><span style="cursor: pointer;color: var(--primary)" class="ly-guide-next">下一条</span> </div> <div x-arrow="" class="popper__arrow" ></div></div>';
            var popover = div.children[0];
            this._popover = popover;
            var titleV = popover.getElementsByClassName('el-popover__title')[0];
            var contentV = popover.getElementsByClassName('popover__content')[0];
            var arrayV = popover.getElementsByClassName('popper__arrow')[0];
            var beforeV = popover.getElementsByClassName('ly-guide-before')[0];
            var nextV = popover.getElementsByClassName('ly-guide-next')[0];
            var closeV = popover.getElementsByClassName('ly-guide-close')[0];
            closeV.style.display = can_finish ? 'inline-block' : 'none';
            closeV.addEventListener('click', function (e) {
                e.stopPropagation();
                if (callback) {
                    var ret = callback(false);
                    if (ret instanceof Promise) {
                        ret.then(function () {
                            me.hide();
                        });
                    } else {
                        me.hide();
                    }
                } else {
                    me.hide();
                }

            });
            beforeV.addEventListener('click', function () {
                showStep(-1);
            });
            var nextClick = function () {
                if (step + 1 === _guides.length) {
                    me.hide();
                    if (callback) {
                        callback(true);
                    }
                } else {
                    showStep(1);
                }
            };
            _nextClickModel.addEventListener('click', nextClick);
            nextV.addEventListener('click', nextClick);
            popover.remove();
            document.body.append(popover);

            function getTop(e) {
                var offset = e.offsetTop;
                if (e.offsetParent != null) offset += getTop(e.offsetParent);
                return offset;
            }

            function getLeft(e) {
                var offset = e.offsetLeft;
                if (e.offsetParent != null) offset += getLeft(e.offsetParent);
                return offset;
            }

            function getStyle(obj, arr) {
                if (obj.currentStyle) {
                    return obj.currentStyle[arr];  //针对ie
                } else {
                    return document.defaultView.getComputedStyle(obj, null)[arr];
                }
            }

            function getScrollParent(element) {
                var parent = element.parentNode;
                if (!parent) {
                    return element;
                }
                if (parent === document) {
                    return null;
                }
                if (
                    ['scroll', 'auto'].indexOf(getStyle(parent, 'overflow')) !== -1 ||
                    ['scroll', 'auto'].indexOf(getStyle(parent, 'overflow-x')) !== -1 ||
                    ['scroll', 'auto'].indexOf(getStyle(parent, 'overflow-y')) !== -1
                ) {
                    return parent;
                }
                return getScrollParent(element.parentNode);
            }

            function scrollIntoView(parent, tragetElem) {
                const tragetElemPostition = getTop(tragetElem)-400;
                // 当前滚动高度
                let scrollTop =
                    document.documentElement.scrollTop || document.body.scrollTop;
                // 滚动step方法
                const step = function () {
                    let distance = tragetElemPostition - scrollTop;
                    scrollTop = scrollTop + distance / 5;
                    if (Math.abs(distance) < 1) {
                        parent.scrollTo(0, tragetElemPostition);
                    } else {
                        parent.scrollTo(0, scrollTop);
                        setTimeout(step, 20);
                    }
                };
                step();
            }

            var bef_parent = null;
            var left = 0;
            var top = 0;
            var top_el = 0;
            var left_el = 0;
            var scorllListener = function (event) {
                popover.style.top = top - event.target.scrollTop + 'px';
                popover.style.left = left - event.target.scrollLeft + 'px';
                child.style.top = top_el - event.target.scrollTop + 'px';
                child.style.left = left_el - event.target.scrollLeft + 'px';
            };

            function showStep(add) {
                if(bef_parent){
                    bef_parent.removeEventListener('scroll',scorllListener)
                }
                if (step > -1) {
                    var bef_el = _guides[step];
                    bef_el.classList.remove('ly-guide-show');
                }
                step += add;
                if (step + 1 > _guides.length) {
                    document.body.style.overflow="";
                    return;
                }
                var guide_el = _guides[step];
                left = getLeft(guide_el);
                top = getTop(guide_el);
                left_el = left;
                top_el = top;
                var placement = guide_el.getAttribute('guide-placement');
                var content = guide_el.getAttribute('guide-content');
                var title = guide_el.getAttribute('guide-title');
                var background = guide_el.getAttribute('guide-background');
                if (background) {
                    guide_el.style.background = background;
                }
                titleV.innerText = title;
                contentV.innerHTML = content;
                titleV.style.display = title ? 'block' : 'none';
                beforeV.style.display = step === 0 ? 'none' : 'inline-block';
                nextV.innerText = step + 1 < _guides.length ? '下一条' : '完成';

                child.style.width = guide_el.offsetWidth + 'px';
                child.style.height = guide_el.offsetHeight + 'px';
                child.style['border-radius'] = getStyle(guide_el, 'border-radius');
                if (!placement) {
                    placement = 'bottom';
                }
                if (placement === 'bottom') {
                    top += guide_el.offsetHeight;
                }
                if (placement === 'top') {
                    top -= popover.offsetHeight + 10;
                }
                if (placement === 'left') {
                    left -= popover.offsetWidth + 10;
                }
                if (placement === 'right') {
                    left += guide_el.offsetWidth + 10;
                }
                arrayV.style.top = '';
                arrayV.style.left = '';
                if ((placement === 'top' || placement === 'bottom') && guide_el.offsetWidth < popover.offsetWidth) {
                    arrayV.style.left = parseInt(guide_el.offsetWidth / 2) + 'px';
                }
                if ((placement === 'left' || placement === 'right') && guide_el.offsetHeight < popover.offsetHeight) {
                    arrayV.style.top = parseInt(guide_el.offsetHeight / 2) + 'px';
                }
                popover.setAttribute('x-placement', placement);
                popover.style.left = left + 'px';
                popover.style.top = top + 'px';
                child.style.left = left_el + 'px';
                child.style.top = top_el + 'px';
                guide_el.classList.add('ly-guide-show');
                var parent = getScrollParent(guide_el);
                if (parent) {
                    parent.addEventListener('scroll',scorllListener);
                    bef_parent = parent;
                    document.body.style.overflow="hidden";
                    scrollIntoView(parent, guide_el);
                } else {
                    document.body.style.overflow="";
                    scrollIntoView(window, guide_el);
                }
            }
            showStep(1);
        }, hide: function () {
            _is_show = false;
            for (let i=0;i < _option.guides.length;i++) {
                let info = _option.guides[i];
                let el=info.el;
                if(!el){
                    el = document.querySelectorAll([info.selector])[0];
                }
                if(!el){
                    continue;
                }
                el.removeAttribute('guide-content', info.content);
                el.removeAttribute('guide-placement',info.placement);
                el.removeAttribute('guide-title',info.title);
            }
            _modelV.remove();
            _nextClickModel.remove();
            this._popover.remove();
            for (let i = 0; i < _guides.length; i++) {
                let guide_el = _guides[i];
                guide_el.classList.remove('ly-guide-show');
            }
        }
    }
});