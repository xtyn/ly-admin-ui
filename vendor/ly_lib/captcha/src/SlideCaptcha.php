<?php

namespace lylib\captcha;

use lylib\captcha\EncryptUtil;
use rap\cache\Cache;

class SlideCaptcha
{


    public function check($verify_code,$sid)
    {
        $key=md5(request()->session()->sessionId() . ':SlideCaptcha'.$sid);
        $item = Cache::get($key);
        Cache::remove($key);
        $sign = $item[0];
        $x = $item[1];
        $item_md5 = $item[2];
        $code = EncryptUtil::decrypt($verify_code, $sign);
        $item_md51 = substr($code, 0, 32);
        $time = ((int)substr($code, 32, 13)) / 1000;
        $x1 = substr($code, 45);
        if ($item_md5 == $item_md51 && abs(time() - $time) < 30 && abs($x1 - $x) < 5) {
            return true;
        }
        return false;
    }


    /**
     * 构造图片
     * @param $srcFile
     * @return false|GdImage|resource
     */
    private function createImg($srcFile)
    {
        $data = getimagesize($srcFile);
        switch ($data['2']) {
            case 1:
                $im = imagecreatefromgif($srcFile);
                break;
            case 2:
                $im = imagecreatefromjpeg($srcFile);
                break;
            case 3:
                $im = imagecreatefrompng($srcFile);
                break;
            case 6:
                $im = imagecreatefromwbmp($srcFile);
                break;
        }
        return $im;
    }


    /**
     * 在图片上截取一个正方形
     * @param $uimg
     * @param $x
     * @param $y
     * @param $width
     * @param $height
     * @return false|GdImage|resource
     */
    private function crop($uimg, $x, $y, $width, $height)
    {
        $new_image = imagecreatetruecolor($width, $height);
        imagecopyresampled($new_image, $uimg, 0, 0, $x, $y, $width, $height, $width, $height);
        imagedestroy($uimg);
        return $new_image;
    }

    /**
     * 通过
     * @param $userImgUrl
     * @param $templateImgUrl
     * @param $x
     * @param $y
     * @return false|GdImage|resource
     */
    public function cropImgByTemplate($userImgUrl, $templateImgUrl, $x, $y)
    {
        list($width, $height) = getimagesize($templateImgUrl);
        list($w, $h) = getimagesize($userImgUrl);
        $uimg = $this->createImg($userImgUrl);
        $uim = $this->crop($uimg, $x, $y, $width, $height);
        $tempImg = imagecreatefrompng($templateImgUrl);
        $img = imagecreatetruecolor($width, $height); // 创建一个底图，大小和模板一致
        imagesavealpha($img, true); // 设置alpha通道为true，显示透明部分
        $bg = imagecolorallocatealpha($img, 255, 255, 255, 127); // 摄取一个透明的颜色
        imagefill($img, 0, 0, $bg); // 将透明颜色填满底图，得到一张全透明的图片
        for ($i = 0; $i < $width; $i++) { // 遍历图片的像素点
            for ($j = 0; $j < $height; $j++) {
                $rgb = imagecolorat($tempImg, $i, $j); // 获取模板上某个点的色值
                if (!$rgb) { // 不是透明的
                    $color = imagecolorat($uim, $i, $j); // 在用户的图片上对应的地方取色值
                    imagesetpixel($img, $i, $j, $color); // 在透明底图上画出要裁剪出来的部分
                }
            }
        }
        imagedestroy($uimg);
        imagedestroy($uim);
        imagedestroy($tempImg);
        return $img;
    }


    public function cropItem($x, $y, $bg_url)
    {
        $dir = __DIR__ . '/assets/slide/';
        $tempUrl = $dir . '3.png';
        $img = $this->cropImgByTemplate($bg_url, $tempUrl, $x, $y);
        $b2 = $dir . '1.png';
        $back = imagecreatefrompng($b2);
        imagesavealpha($back, true);
        $bg = imagecolorallocatealpha($img, 255, 255, 255, 127); // 摄取一个透明的颜色
        imagefill($img, 0, 0, $bg); // 将透明颜色填满底图，得到一张全透明的图片
        $waterMarkWidth = imagesx($img);
        $waterMarkHeight = imagesy($img);
        imagecopy($back, $img, 0, 0, 0, 0, $waterMarkWidth, $waterMarkHeight);
        ob_start();
        imagepng($back);
        $content = ob_get_clean();
        imagedestroy($img);
        imagedestroy($back);
        return $content;
    }

    public function background($x, $y, $bg_url)
    {
        $dir = __DIR__ . '/assets/slide/';
        $v = $dir . '2.png';
//        $url =$dir.'bg/bg1.png';
        $back = $this->createImg($bg_url);
        $img = imagecreatefrompng($v);
        $waterMarkWidth = imagesx($img);
        $waterMarkHeight = imagesy($img);
        imagecopy($back, $img, $x, $y, 0, 0, $waterMarkWidth, $waterMarkHeight);
        ob_start();
        imagepng($back);
        $content = ob_get_clean();
        imagedestroy($img);
        imagedestroy($back);
        return $content;
    }

    public function build($sid=1)
    {
        $x = rand(100, 230);
        $y = rand(20, 80);
        $dir = __DIR__ . '/assets/slide/';
        $bg = rand(1, 6);
        $bg_url = $dir . 'bg/bg' . $bg . '.png';
        $item = $this->cropItem($x, $y, $bg_url);
        $background = $this->background($x, $y, $bg_url);
        $item = 'data:image/png;base64,' . chunk_split(base64_encode($item));
        $background = 'data:image/png;base64,' . chunk_split(base64_encode($background));
        $sign = md5(time());
        $item_md5 = md5($item);
        $item = EncryptUtil::encrypt($item, $sign);
        $background = EncryptUtil::encrypt($background, $sign);
        $code = substr($sign, 0, 16) . $background . ' ' . substr($sign, 16) . $item . ' ' . $item_md5 . $y;
        $key=md5(request()->session()->sessionId() . ':SlideCaptcha'.$sid);
        Cache::set($key, [$sign, $x, $item_md5], 600);
        return ['code' => $code];
    }
}
