<?php


namespace lylib\captcha;


use rap\cache\Cache;

class MoveCaptcha {

    public function build($_sid=1) {
        $dir = __DIR__ . '/assets/move/ball';
        $tempImage = $dir . '/background.png'; //模板1
        $targetImg = $dir . '/target.png'; //模板1
        $info = file_get_contents( $dir . '/info.txt');
        $bgImg = imagecreatefromstring(file_get_contents($tempImage));
        $targetImg = imagecreatefromstring(file_get_contents($targetImg));
        $bgWidth = imagesx($bgImg);
        $bgHeight = imagesy($bgImg);
        $targetWidth = imagesx($targetImg);
        $targetHeight = imagesy($targetImg);
        $left = mt_rand($bgWidth/3, $bgWidth-$targetWidth);
        $top =mt_rand(0, $bgHeight-$targetHeight);
        $img = imagecreatetruecolor($bgWidth, $bgHeight);
        $alpha = imagecolorallocatealpha($img, 0, 0, 0, 127);
        imagefill($img, 0, 0, $alpha);
        imagecopyresampled($img, $bgImg, 0, 0, 0, 0, $bgWidth, $bgHeight, $bgWidth, $bgHeight);
        imagecopyresampled($img, $targetImg,  $left ,   $top, 0, 0, $targetWidth, $targetHeight, $targetWidth, $targetHeight);
        ob_start();
        imagepng($img);
        $content = ob_get_clean();
        $base64_text = 'data:image/png;base64,' . chunk_split(base64_encode($content));
        imagedestroy($img);
        imagedestroy($bgImg);
        imagedestroy($targetImg);
        $base64_move = 'data:image/png;base64,' . chunk_split(base64_encode(file_get_contents($dir . '/move.png')));
        $sign = md5(time());
        $base64_text = EncryptUtil::encrypt($base64_text, $sign);
        $base64_move = EncryptUtil::encrypt($base64_move, $sign);
        $cache_key=md5(request()->session()->sessionId() . '_MoveCaptcha'.$_sid);

        Cache::set($cache_key, [$sign, $left+$targetWidth/2, $top+$targetHeight/2], 600);
        $code = substr($sign, 0, 16) . $base64_text . ' ' . substr($sign, 16) . $base64_move;
        return ['code' => $code,'info'=>$info,'tmp'=>[$left+$targetWidth/2, $top+$targetHeight/2]];
    }

    public function check($value,$_sid=1) {
        $cache_key=md5(request()->session()->sessionId() . '_MoveCaptcha'.$_sid);
        $kv = Cache::get($cache_key);
        Cache::remove($cache_key);
        if (!$kv) {
          return  false;
        }

        $sign = $kv[ 0 ];
        $data = explode(' ', $value);
        $time = ((int)substr(EncryptUtil::decrypt($data[ 0 ], $sign),32))/ 1000;;
        $x = (float) substr(EncryptUtil::decrypt($data[ 1 ], $sign),32);
        $y = (float) substr(EncryptUtil::decrypt($data[ 2 ], $sign),32);
        $x1 = $kv[ 1 ];
        $y1 = $kv[ 2 ];
        if (abs($x-$x1)<5&&abs($y-$y1)<5){
            return true;
        }
        return false;
    }

}