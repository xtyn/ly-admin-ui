<?php

namespace lylib\captcha;

use rap\cache\Cache;

/**
 * 获取验证码
 *
 * @author 藤之内
 * @url    /captcha
 */
class CaptchaController
{


    /**
     * 图片验证码
     * @param int $length 验证码长度
     * @param int $codeSetType
     */
    public function picture($length = 3, $codeSetType = 1)
    {
        if ($length < 3) {//验证码最多3位
            $length = 3;
        }
        if ($length > 5) {//验证码最多5位
            $length = 5;
        }
        $codeSet = '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY';
        if ($codeSetType == 1) {
            $codeSet = '2345678abcdefhijkmnpqrstuvwxyzABCDEFGHJKLMNPQRTUVWXY';
        }
        if ($codeSetType == 2) {
            $codeSet = '2345678';
        }
        $config['length'] = $length;
        $config['codeSet'] = $codeSet;
        $verify = new Captcha($config);
        $verify->send();
    }

    /**
     * 获取图片点选验证码(废弃)
     * @return array
     */
    public function textPick()
    {
        $verify = new PickCaptcha();
        return $verify->build();
    }


    /**
     * 滑动验证 获取密文
     * @param int $_sid
     * @return string[]
     */
    public function slideCode($_sid = 1)
    {
        $ca = new SlideCaptcha();
        return $ca->build($_sid);
    }

    /**
     * 滑动验证 检查结果
     * @param $verify_code
     * @param int $_sid
     * @param string $verify_check
     * @return array|false[]
     */
    public function slideCheck($verify_code, $_sid = 1,$_did='',$sense=false,$verify_check='')
    {
        $ca = new SlideCaptcha();
        $success = $ca->check($verify_code, $_sid);
        return LyVerify::checkResult($success, 'slide',$_did,$sense,$verify_check);
    }

    /**
     * 文字点选 获取密文
     * @param int $_sid
     * @return string[]
     */
    public function textCode($_sid = 1)
    {
        $ca = new PickCaptcha();
        $ca->w = 286;
        $ca->h = 143;
        return $ca->build($_sid);
    }

    /**
     * 文字点选 检查结果
     * @param $verify_code
     * @param int $_sid
     * @param string $bc
     * @return array|false[]
     */
    public function textCheck($verify_code, $_sid = 1,$_did='',$sense=false,$verify_check='')
    {
        $ca = new PickCaptcha();
        $success = $ca->check($verify_code, $_sid);
        return LyVerify::checkResult($success, 'text',$_did,$sense,$verify_check);
    }

    /**
     * 移动验证 获取密文
     * @param int $_sid
     * @return array
     */
    public function moveCode($_sid = 1)
    {
        $ca = new MoveCaptcha();
        return $ca->build($_sid);
    }


    /**
     * 移动验证 检查结果
     * @param $verify_code
     * @param int $_sid
     * @param string $bc
     * @return array|false[]
     */
    public function moveCheck($verify_code, $_sid = 1,$_did='',$sense=false,$verify_check='')
    {
        $ca = new MoveCaptcha();
        $success = $ca->check($verify_code, $_sid);
        return LyVerify::checkResult($success, 'move',$_did,$sense,$verify_check);
    }

    /**
     * 旋转验证 获取密文
     * @param int $_sid
     * @return string[]
     */
    public function rotateCode($_sid = 1)
    {
        $ca = new RotateCaptcha();
        return $ca->build($_sid);
    }

    /**
     * 旋转验证 验证结果
     * @param $verify_code
     * @param int $_sid
     * @param string $bc
     * @return array|false[]
     */
    public function rotateCheck($verify_code, $_sid = 1,$_did='',$sense=false,$verify_check='')
    {
        $ca = new RotateCaptcha();
        $success = $ca->check($verify_code, $_sid);
        return LyVerify::checkResult($success, 'rotate',$_did,$sense,$verify_check);
    }


    /**
     * 验证token 同一个系统可以使用
     * LyVerify::checkToken 进行验证
     * @param $token
     * @return array|false[]|mixed
     */
    public function checkToken($token)
    {
        $val=LyVerify::checkToken($token);
        if($val){
            return  $val;
        }
        return ['success' => false];
    }

    public function senseCheck($token,$_did='',$verify_check=''){
       return LyVerify::senseCheck($token,$_did,$verify_check);
    }


}