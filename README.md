## LY-ADMIN-UI 好用超全的管理后台前端框架

### 介绍
Ly-Admin-ui 好用超全的管理后台前端框架，它基于 vue + element-ui+vue-rap 技术栈。
它使用了最新的前端技术栈，项目在element-ui基础上又添加了超多实用组件和布局;
项目整体使用了最新的vue-rap流应用技术,项目可以不需要构建的情况下直接部署,边使用边下载。

- 演示地址 http://lyadminui.magcloud.net/
- 登录地址 http://lyadminui.magcloud.net/portal/login
- 文档地址 http://lyadminui.magcloud.net/

### 特色功能组件
- N种主布局只有搭配
- 支持菜单,配置项,文档搜索的全局搜索能力
- 支持拖动验证,文字点选等多种行为验证方式
- 好看的操作指引
- 支持drop,剪贴板等使用简单的文件上传
- 懒加载方式的弹出框,不需要将弹出框内的代码写到当前页面
- 一行代码的图表展示
- 超级强大的 table 组件和 table 相关的组件
- 完备的权限验证能力
- 还有很多,太多了...

### 安装方式

------
> 下载文件 [LyAdminUI](http://lyadminui.magcloud.net/lyadminui.zip "LyAdminUI")

### 目录结构

```
├─portal                       前端跟目录
│  ├─comp                      项目特用的组件,且不会抽取为公共的组件
│  ├─utils                     工具文件
│  │   ├─area.json             地区数据(请勿移动)
│  │   ├─state.js              状态量管理
│  │   ├─menus.json            这里是你的菜单配置文件
│  ├─ly                        LYUI 框架目录(请勿移动)
│  ├─其他自定义模块              其他自定义模块
│  ├─global.js                 入口js文件
│  ├─index.html                入口html,正常会由后端渲染返回
├─static                       前端静态文件
│  ├─ly  	                   LYUI 需要使用到的静态文件(请勿移动)
│  ├─imgs	                   静态图片
│  ├─libs	                   引用的三方js 库
```

拉取后的目录结构为推荐的目录结构,可以根据实际情况进行调整

> 标志为请勿移动的目录为 LyUI 框架文件,请勿随意修改或在目录内放入其他项目文件
### 框架更新

------
重新拉取项目,覆盖  `/portal/ly`,`/static/ly`三个目录
### 项目截图
<div align="center">
  <img  width="92%" style="border-radius:10px;margin-top:20px;margin-bottom:20px;box-shadow: 2px 0 6px gray;" src="images/1.jpeg" />
  <img  width="92%" style="border-radius:10px;margin-top:20px;margin-bottom:20px;box-shadow: 2px 0 6px gray;" src="images/2.jpeg" />
  <img  width="92%" style="border-radius:10px;margin-top:20px;margin-bottom:20px;box-shadow: 2px 0 6px gray;" src="images/3.jpeg" />
  <img  width="92%" style="border-radius:10px;margin-top:20px;margin-bottom:20px;box-shadow: 2px 0 6px gray;" src="images/4.jpeg" />

</div>