(function () {
    var inputRender = '<div  class="dh_holder dh_wind dh_wait_compute " style="width: 100%;"><div class="dh_form"><input type="hidden" name="dh_challenge"><input type="hidden"  name="dh_validate"><input  type="hidden" name="dh_seccode"></div><div class="dh_btn"><div class="dh_radar_btn"><div class="dh_radar"><div class="dh_ring"><div class="dh_small"></div></div><div class="dh_sector" style="transform: rotate(85.262deg);"><div class="dh_small"></div></div><div class="dh_cross"><div class="dh_h"></div><div class="dh_v"></div></div><div class="dh_dot"></div><div class="dh_scan"><div class="dh_h"></div></div><div class="dh_status"><div class="dh_bg"></div><div class="dh_hook"></div></div></div><div class="dh_ie_radar"></div><div class="dh_radar_tip" tabindex="0" aria-label="点击按钮完成验证" style="outline-width: 0px;"><span   class="dh_radar_tip_content">点击按钮完成验证</span><span class="dh_radar_error_code"></span></div><div class="dh_other_offline dh_offline"></div></div><div class="dh_ghost_success "><div class="dh_success_btn"><div class="dh_success_box"><div class="dh_success_show"><div class="dh_success_pie"></div><div class="dh_success_filter"></div><div class="dh_success_mask"></div></div><div class="dh_success_correct"><div class="dh_success_icon"></div></div></div><div class="dh_success_radar_tip"><span class="dh_success_radar_tip_content">验证成功</span><span  class="dh_success_radar_tip_timeinfo"></span></div><div class="dh_success_offline dh_offline"></div></div></div><div class="dh_slide_icon"></div></div><div class="dh_wait"><span class="dh_wait_dot dh_dot_1"></span><span  class="dh_wait_dot dh_dot_2"></span><span class="dh_wait_dot dh_dot_3"></span></div><div class="dh_goto" style="display: none;"><div class="dh_goto_ghost"></div><div class="dh_goto_wrap"><div class="dh_goto_content"><div class="dh_goto_content_tip"></div></div><div class="dh_goto_cancel"></div><a class="dh_goto_confirm"></a></div></div><div class="dh_panel"><div class="dh_panel_ghost"></div><div class="dh_panel_box"><div class="dh_other_offline dh_panel_offline"></div><div class="dh_panel_loading"><div class="dh_panel_loading_icon"></div><div class="dh_panel_loading_content"></div></div><div class="dh_panel_success"><div class="dh_panel_success_box"><div class="dh_panel_success_show"><div class="dh_panel_success_pie"></div><div class="dh_panel_success_filter"></div><div class="dh_panel_success_mask"></div></div><div class="dh_panel_success_correct"><div class="dh_panel_success_icon"></div></div></div><div class="dh_panel_success_title"></div></div><div class="dh_panel_error"><div class="dh_panel_error_icon"></div><div class="dh_panel_error_title"></div><div class="dh_panel_error_content"></div><div class="dh_panel_error_code"></div></div><div class="dh_panel_footer"><div class="dh_panel_footer_logo"></div><div class="dh_panel_footer_copyright"></div></div><div class="dh_panel_next"></div></div></div></div>';
    var panelRender = '<div  class="dh_fullpage_click dh_float dh_wind dh_click"  style="display: none; opacity: 1;"><div class="dh_fullpage_ghost"></div><div class="dh_fullpage_click_wrap"><div class="dh_fullpage_click_box" ><div class="dh_holder dh_silver" style="display: block; opacity: 1;"><div class="dh_widget"><div class="dh_head"><div class="dh_tips"><div class="dh_tip_content">请在下图<span class="dh_mark">依次</span>点击：</div><img  class="dh_tip_img" aria-hidden="true" style="width: 116px; height: 40px; right: -116px; top: -10px; display: block;"/></div><div class="dh_tip_space"></div></div><div class="dh_table_box"><div class="dh_window"><div class="dh_item dh_big_item" style="width: 95.8%; padding-bottom: 95.8%;"><div class="dh_item_loading"><div class="dh_item_loading_icon"></div><div class="dh_item_loading_tip">加载中...</div></div><div class="dh_item_wrap"><img  class="dh_item_img"   style="right: 0px; top: 0px; width: 100%;"></div></div></div><div  class="dh_result_tip"></div></div><div class="dh_panel"><div class="dh_small" tabindex="-1" style="outline: none; font-size: 12px;"><a   class="dh_close"  tabindex="-1"><div class="dh_close_tip">关闭验证</div></a><a   class="dh_refresh" href="javascript:;" tabindex="-1"><div class="dh_refresh_tip">刷新验证</div></a><a class="dh_voice"><div class="dh_voice_tip"></div></a></div><a  class="dh_commit" href="javascript:;"><div  class="dh_commit_tip">确认</div></a></div></div></div></div><div class="dh_fullpage_pointer" style="display: block;"><div class="dh_fullpage_pointer_out"></div><div class="dh_fullpage_pointer_in"></div></div></div></div>';
    var defaultConfig = {
        el: '',
        showInput: true,
        codeUrl: '/api/open/testd',
        checkUrl: '/api/open/check',
        size: 3,
        lang: {
            input: '点击按钮完成验证',
            success: '验证成功'
        },
        checkShow: function () {
            return true;
        }, verifySuccess: function () {

        },verifyFail:function () {

        }
    };

    window.RapVerify = {
        render: function (c) {
            return init(c);
        }
    };

    var is_mobile = /(iPhone|iPad|iPod|iOS|Android)/i.test(navigator.userAgent);

    function init(c) {
        var sign_key = '';
        var $input = null;
        var $panel = null;
        var $dh_fullpage_click_box = null;
        var $dh_close = null;
        var $dh_fullpage_ghost = null;
        var $dh_item_img;
        var $dh_item;
        var $dh_tip_img;
        var verify_result;
        var is_success = false;
        var $dh_fullpage_click;
        var lang = $.extend(defaultConfig.lang, c.lang);
        var config = $.extend({},defaultConfig, c);
        config.lang = lang;
        var result = [];
        var begin_time = 0;


        function createInput() {
            $input = $(inputRender);
            $(config.el).html('').append($input);
            $input.find('.dh_radar_tip_content').text(config.lang.input);
            $input.find('.dh_success_radar_tip_content').text(config.lang.success);
            $input.hover(function () {
                $(this).addClass('dh_wait_compute');
            }, function () {
                $(this).removeClass('dh_wait_compute');
            });
            $input.click(function () {
                if (is_success)return;
                if (config.checkShow && !config.checkShow()) {
                    return;
                }
                $(this).addClass('dh_radar_click_ready');
                var offset = $(this).offset();
                showVerifyPanel(offset);

            });
        }

        function showVerifyPanel(offset) {
            if (is_success)return;
            createPanel();
            $panel.show();
            loadVerify();

            if (is_mobile) {
                $dh_fullpage_click_box.addClass('dh_popup');
                $dh_fullpage_click_box.css({'width': '100%'});
                $dh_fullpage_click.css({'box-shadow': 'none', 'border': '0'});
                $panel.addClass('dh_popup');
                $panel.css('position', 'fixed');

                $panel.find('.dh_fullpage_click_wrap').css({'margin-top': '-55%', 'width': '90%'});
                $panel.find('.dh_fullpage_pointer').hide();
            } else {
                if (!offset) {
                    offset = {left: $(window).width() / 2 - 336 / 2, top: $(window).height() / 2};
                    $panel.find('.dh_fullpage_pointer').hide();
                } else {
                    $panel.find('.dh_fullpage_pointer').show();
                }
                offset.top = offset.top + 13;
                offset.left = offset.left + 50;
                $panel.css(offset);
                var mtop = 180;
                if (offset.top < 200) {
                    mtop = offset.top - 20;
                }
                $panel.find('.dh_holder.dh_silver').width(336);
                $dh_fullpage_click_box.css({top: -mtop})
            }
        }

        if (config.showInput) {
            createInput();
        }

        function loadVerify() {
            $.get(config.codeUrl, function (rs) {
                var code = rs.code;
                var kv = code.split(' ');
                sign_key = kv[0].substr(0, 16) + kv[1].substr(0, 16);
                $dh_item_img.attr('src', b(kv[0].substr(16), sign_key));
                $dh_tip_img.attr('src', b(kv[1].substr(16), sign_key));
                $panel.find('.dh_big_mark').remove();
                result.length = 0;
                begin_time = new Date().getTime();
            });
        }

        function createPanel() {
            if ($panel)return;
            $panel = $(panelRender);
            $dh_fullpage_click_box = $panel.find('.dh_fullpage_click_box');
            $dh_fullpage_click = $panel.find('.dh_fullpage_click');
            $dh_close = $panel.find('.dh_close');
            $dh_fullpage_ghost = $panel.find('.dh_fullpage_ghost');
            $dh_item_img = $panel.find('.dh_item_img');
            $dh_item = $panel.find('.dh_item');
            $dh_tip_img = $panel.find('.dh_tip_img');


            $(document.body).append($panel);


            $dh_close.click(function () {
                $panel.hide();
                if ($input) {
                    $input.removeClass('dh_radar_click_ready');
                }
            });

            $dh_fullpage_ghost.click(function () {
                $dh_close.click();
            });

            $panel.find('.dh_refresh').click(function () {
                loadVerify();
            });

            $dh_item_img.click(function (event) {
                if (result.length + 1 > config.size) {
                    return;
                }
                var x = event.offsetX;
                var y = event.offsetY;
                var w = $dh_item_img.width();
                var p1 = x / w * 10000000000000000;
                var p2 = y / w * 10000000000000000;
                result.push(a(p1 + '', sign_key) + ' ' + a(p2 + '', sign_key));
                var m = '<div mark-show-index="' + result.length + '" class="dh_big_mark dh_mark_show" style="left: ' + x / w * 100 + '%; top: ' + y / w * 100 + '%; transform: translateZ(0px);"><div class="dh_mark_no">' + result.length + '</div></div>';
                m = $(m);
                $dh_item.append(m);
                m.click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr('mark-show-index');
                    for (var i = id; i <= config.size; i++) {
                        $panel.find('.dh_big_mark[mark-show-index=' + i + ']').remove();
                    }
                    result.length = id - 1;
                });
            });

            $panel.find('.dh_commit').click(function () {
                var str = result.join(' ');
                var params = {};
                if (config.verifyCheckParams) {
                    params = config.verifyCheckParams();
                }
                params = $.extend(params, {verify_code: str});

                $.post(config.checkUrl,params, function (rs) {
                    if(!rs.success&&rs.code!='101132'){
                        if (config.verifyFail) {
                            config.verifyFail(rs);
                        }
                        $dh_close.click();
                    }else{
                        var tipV = $panel.find('.dh_result_tip');
                        tipV.addClass('dh_up');
                        tipV.addClass(rs.success ? 'dh_success' : 'dh_fail');
                        var time = parseInt(((new Date().getTime() - begin_time) / 1000));
                        tipV.text(rs.success ? '验证成功,花了' + time + 's,超过9' + Math.ceil(Math.random() * 9) + '%的用户' : '验证失败,请按提示重新操作');
                        setTimeout(function () {
                            tipV.removeClass('dh_up');
                            tipV.removeClass('dh_success');
                            if (rs.success) {
                                verify_result = str;
                                if ($input) {
                                    $input.find('.dh_ghost_success').addClass('dh_success_animate');
                                }
                                $dh_close.click();
                                is_success = true;
                                if (config.verifySuccess) {
                                    config.verifySuccess(rs);
                                }
                            } else {
                                loadVerify();
                                result = [];
                                $panel.find('.dh_big_mark').remove();
                            }
                        }, 1000);
                    }

                });
            });
        }

        return {
            getCode: function () {
                return verify_result;
            },
            isVerify: function () {
                return is_success;
            },
            showVerifyPanel: function (offset) {
                showVerifyPanel(offset);
            }
        }
    }

    function a(str, sign_key) {
        var sign_b = c(sign_key);
        var str_b = c(str);
        var b = [];
        for (var i = 0; i < str_b.length; i++) {
            var k = str_b[i] ^ sign_b[i % sign_b.length];
            b.push(k);
        }
        return window.btoa(d(b));
    }

    function b(str, sign_key) {
        str = window.atob(str);
        var sign_b = c(sign_key);
        var str_b = c(str);
        var b = [];
        for (var i = 0; i < str_b.length; i++) {
            var k = str_b[i] ^ sign_b[i % sign_b.length];
            b.push(k);
        }
        return d(b);
    }


    function c(str) {
        var bytes = [];
        var len, c;
        len = str.length;
        for (var i = 0; i < len; i++) {
            c = str.charCodeAt(i);
            if (c >= 0x010000 && c <= 0x10FFFF) {
                bytes.push(((c >> 18) & 0x07) | 0xF0);
                bytes.push(((c >> 12) & 0x3F) | 0x80);
                bytes.push(((c >> 6) & 0x3F) | 0x80);
                bytes.push((c & 0x3F) | 0x80);
            } else if (c >= 0x000800 && c <= 0x00FFFF) {
                bytes.push(((c >> 12) & 0x0F) | 0xE0);
                bytes.push(((c >> 6) & 0x3F) | 0x80);
                bytes.push((c & 0x3F) | 0x80);
            } else if (c >= 0x000080 && c <= 0x0007FF) {
                bytes.push(((c >> 6) & 0x1F) | 0xC0);
                bytes.push((c & 0x3F) | 0x80);
            } else {
                bytes.push(c & 0xFF);
            }
        }
        return bytes;
    }

    function d(arr) {
        if (typeof arr === 'string') {
            return arr;
        }
        var str = '',
            _arr = arr;
        for (var i = 0; i < _arr.length; i++) {
            var one = _arr[i].toString(2),
                v = one.match(/^1+?(?=0)/);
            if (v && one.length == 8) {
                var bytesLength = v[0].length;
                var store = _arr[i].toString(2).slice(7 - bytesLength);
                for (var st = 1; st < bytesLength; st++) {
                    store += _arr[st + i].toString(2).slice(2);
                }
                str += String.fromCharCode(parseInt(store, 2));
                i += bytesLength - 1;
            } else {
                str += String.fromCharCode(_arr[i]);
            }
        }
        return str;
    }


})();